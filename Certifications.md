```mermaid
gantt
    title Timeline certifications
    dateFormat DD-MM-YYYY
    section Certifications
        ISC2 Certified in Cybersecurity :a1, 2024/11/01, 30d
        eCPPTv2 : after a1, 90d

```

Récapitulatif des certifications prévues :

RED :
- [eCPPTv2](https://elearnsecurity.com/product/) 
- [eWPT](https://elearnsecurity.com/product/ewpt-certification/)
- [HTB Certified Penetration Testing Specialist (HTB CPTS)](https://academy.hackthebox.com/preview/certifications/htb-certified-penetration-testing-specialist)
- [OSEP](https://www.offensive-security.com/learn-one/pen300osep/?utm_source=adwords&utm_term=osep&utm_campaign=&utm_medium=ppc&hsa_mt=b&hsa_ad=554438181596&hsa_net=adwords&hsa_src=g&hsa_kw=osep&hsa_tgt=kwd-106538855&hsa_cam=15001439649&hsa_acc=7794287291&hsa_ver=3&hsa_grp=137237850228&gclid=EAIaIQobChMIyqWA7cnN9gIV7JBoCR3dQwiJEAAYASAAEgKc5vD_BwE) 
- [GIAC Security Professional (GSP)](https://www.giac.org/get-certified/giac-portfolio-certifications/?msc=main-nav)
- [GIAC Penetration Tester Certification (GPEN)](https://www.giac.org/certifications/penetration-tester-gpen/) 
- [GIAC Cloud Penetration Tester (GCPN)](https://www.giac.org/certifications/cloud-penetration-tester-gcpn/) 
- [GIAC Web Application Penetration Tester (GWAPT)](https://www.giac.org/certifications/web-application-penetration-tester-gwapt/)
- [GIAC Experienced Cybersecurity Specialist Certification (GX-CS)](https://www.giac.org/certifications/experienced-cyber-security-gxcs/) 
- [GIAC Experienced Intrusion Analyst Certification (GX-IA)](https://www.giac.org/certifications/experienced-intrusion-analyst-gxia/) 


BLUE :
- [BTL1](https://securityblue.team/why-btl1/)
- [eCIR](https://elearnsecurity.com/product/ecir-certification/) 
- [eCDFP](https://elearnsecurity.com/product/ecdfp-certification/) 
- [eCMAP](https://elearnsecurity.com/product/ecmap-certification/) 
- [eCTHPv2](https://elearnsecurity.com/product/ecthpv2-certification/)
- [CPTA](https://www.cyberwarfare.live/courses/certified-purple-team-analyst) 